Source: flask-dance
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Sergio de Almeida Cipriano Junior <sergiosacj@riseup.net>,
           Gabriela Pivetta <gpivetta99@gmail.com>,
           João Pedro <joaopedrohm17@gmail.com>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 flit,
 python3-setuptools,
 python3-all,
 python3-requests-oauthlib,
 python3-requests,
 python3-oauthlib,
 python3-flask,
 python3-werkzeug,
 python3-urlobject,
 python3-sphinx <!nodoc>,
 python3-pytest <!nocheck> <!nodoc>,
 python3-pytest-mock <!nocheck>,
 python3-responses <!nocheck>,
 python3-freezegun <!nocheck>,
 python3-coverage <!nocheck>,
 python3-flask-sqlalchemy <!nocheck>,
 python3-sqlalchemy <!nocheck> <!nodoc>,
 python3-flask-login <!nocheck>,
 python3-flask-caching <!nocheck>,
 python3-betamax <!nocheck> <!nodoc>,
 python3-funcparserlib <!nodoc>,
 python3-sphinxcontrib.spelling <!nodoc>,
 python3-sphinxcontrib.seqdiag <!nodoc>,
 python3-enchant <!nodoc>,
 python3-docs-theme <!nodoc>,
 libjs-underscore <!nodoc>,
 libjs-jquery <!nodoc>,
 sphinx-common <!nodoc>,
 python3-blinker <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://flask-dance.readthedocs.io/en/latest/
Vcs-Browser: https://salsa.debian.org/python-team/packages/flask-dance
Vcs-Git: https://salsa.debian.org/python-team/packages/flask-dance.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-flask-dance
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends}
Suggests: python-flask-dance-doc
Description: Connect your Flask app with OAuth
 Flask-Dance does the "OAuth dance" using Flask, requests, and oauthlib. It is
 designed to allow users to authenticate via OAuth protocol on Flask-based
 applications. It is built using blueprints, therefore supports all the features
 that any blueprint supports, including registering the blueprint at any URL
 prefix or subdomain you want, url routing, and custom error handlers.
 .
 This package installs the library for Python 3.

Package: python-flask-dance-doc
Architecture: all
Section: doc
Depends:
 ${sphinxdoc:Depends},
 ${misc:Depends}
Description: Connect your Flask app with OAuth (common documentation)
 Flask-Dance does the "OAuth dance" using Flask, requests, and oauthlib. It is
 designed to allow users to authenticate via OAuth protocol on Flask-based
 applications. It is built using blueprints, therefore supports all the features
 that any blueprint supports, including registering the blueprint at any URL
 prefix or subdomain you want, url routing, and custom error handlers.
 .
 This is the common documentation package.
